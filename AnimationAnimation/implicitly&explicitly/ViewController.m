//
//  ViewController.m
//  implicitly&explicitly
//
//  Created by shinyway on 2018/11/7.
//  Copyright © 2018年 shinyway. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *LayerView;
@property (weak, nonatomic) IBOutlet UIButton *chageColorButton;

/** layer */
@property(nonatomic, strong)CALayer *colorLayer;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.colorLayer = [CALayer layer];
    self.colorLayer.frame = CGRectMake(0, 0, 100, 100);
    self.colorLayer.position = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
    self.colorLayer.backgroundColor = [UIColor redColor].CGColor;
    
    //add custom action
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromLeft;
    self.colorLayer.actions = @{@"backgroundColor": transition};
    
    [self.view.layer addSublayer:self.colorLayer];
    
    [self.chageColorButton addTarget:self action:@selector(ChageColor) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapView:)];
    [self.view addGestureRecognizer:tap];
    
}


- (void)ChageColor{
   
    
    /* //begin a new transaction
    [CATransaction begin];
    //set the animation duration to 1 second
    [CATransaction setAnimationDuration:1.0];
    //add animation on completion
    [CATransaction setCompletionBlock:^{
        CATransform3D transform = self.colorLayer.transform;
        transform = CATransform3DRotate(transform, M_PI_2, 0, 0, 1);
        self.colorLayer.transform = transform;
    }];
    //randomize the layer background color
    CGFloat red = arc4random() / (CGFloat)INT_MAX;
    CGFloat green = arc4random() / (CGFloat)INT_MAX;
    CGFloat blue = arc4random() / (CGFloat)INT_MAX;
    self.LayerView.layer.backgroundColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0].CGColor;
    red = arc4random() / (CGFloat)INT_MAX;
    green = arc4random() / (CGFloat)INT_MAX;
    blue = arc4random() / (CGFloat)INT_MAX;
    self.colorLayer.backgroundColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0].CGColor;
    [CATransaction commit];*/
    
    
    CGFloat red = arc4random() / (CGFloat)INT_MAX;
    CGFloat green = arc4random() / (CGFloat)INT_MAX;
    CGFloat blue = arc4random() / (CGFloat)INT_MAX;
    self.colorLayer.backgroundColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0].CGColor;
}

- (void)tapView:(UITapGestureRecognizer *)tap{
    CGPoint point = [tap locationInView:self.view];
    NSLog(@"%f-%f",point.x,point.y);
    //check if we've tapped the moving layer
    if ([self.colorLayer.presentationLayer hitTest:point]) {
        //randomize the layer background color
        CGFloat red = arc4random() / (CGFloat)INT_MAX;
        CGFloat green = arc4random() / (CGFloat)INT_MAX;
        CGFloat blue = arc4random() / (CGFloat)INT_MAX;
        self.colorLayer.backgroundColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0].CGColor;
    } else {
        //otherwise (slowly) move the layer to new position
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        self.colorLayer.position = point;
        NSLog(@"colorLayer-%f-%f",self.colorLayer.position.x,self.colorLayer.position.y);
        [CATransaction commit];
    }
}






@end
