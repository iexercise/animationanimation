//
//  AppDelegate.h
//  implicitly&explicitly
//
//  Created by shinyway on 2018/11/7.
//  Copyright © 2018年 shinyway. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

