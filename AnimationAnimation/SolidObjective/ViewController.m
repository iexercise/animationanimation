//
//  ViewController.m
//  SolidObjective
//
//  Created by shinyway on 2018/11/2.
//  Copyright © 2018年 shinyway. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    UIView *_anchorView;
    UIView *_redView;
    UIView *_blueView;
    UIView *_orangeView;
    UIView *_blankView;
    UIView *_purpleView;
    UIView *_yellowView;
    
}

@property (nonatomic, assign) CGPoint diceAngle;
@end

@implementation ViewController


- (void)addSolidFace:(UIView *)face WithTransform:(CATransform3D )transform{
    [self.anchorView addSubview:face];
    CGSize anchorSize = self.anchorView.bounds.size;
    face.center = CGPointMake(anchorSize.width/2.0, anchorSize.height/2.0);
    face.layer.transform = transform;
    face.layer.doubleSided = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //添加锚点视图
    [self.view addSubview:self.anchorView];
    //设置锚点视图透视
    CATransform3D perspective = CATransform3DIdentity;
    perspective.m34 = -1.0 / 500.0;
//    self.anchorView.layer.transform = perspective;
    
    //添加子视图
    CATransform3D transform = CATransform3DIdentity;
    transform = CATransform3DMakeTranslation(0, 0, 50);
    _redView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    _redView.backgroundColor = [UIColor redColor];
    [self addSolidFace:_redView WithTransform:transform];
    
    
    transform = CATransform3DMakeTranslation( 50, 0, 0);
    transform = CATransform3DRotate(transform, M_PI_2, 0, 1, 0);
    _orangeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    _orangeView.backgroundColor = [UIColor orangeColor];
    [self addSolidFace:_orangeView WithTransform:transform];
    
    transform = CATransform3DMakeTranslation( -50, 0, 0);
    transform = CATransform3DRotate(transform, -M_PI_2, 0, 1, 0);
    _blankView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    _blankView.backgroundColor = [UIColor blackColor];
    [self addSolidFace:_blankView WithTransform:transform];
    
    
    CATransform3D transform4 = CATransform3DIdentity;
    transform4 = CATransform3DMakeTranslation( 0, 50, 0);
    transform4 = CATransform3DRotate(transform4, -M_PI_2, 1, 0, 0);
    _blueView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    _blueView.backgroundColor = [UIColor blueColor];
    [self addSolidFace:_blueView WithTransform:transform4];
    
    
    transform = CATransform3DMakeTranslation( 0, -50, 0);
    transform = CATransform3DRotate(transform, M_PI_2, 1, 0, 0);
    _purpleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    _purpleView.backgroundColor = [UIColor purpleColor];
    [self addSolidFace:_purpleView WithTransform:transform];
    
    transform = CATransform3DMakeTranslation( 0, 0, -50);
    transform = CATransform3DRotate(transform, M_PI, 1, 0, 0);
    _yellowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    _yellowView.backgroundColor = [UIColor yellowColor];
    [self addSolidFace:_yellowView WithTransform:transform];
    
    
    perspective = CATransform3DRotate(perspective, -M_PI_4, 1, 0, 0);
    perspective = CATransform3DRotate(perspective, -M_PI_4, 0, 1, 0);
    self.anchorView.layer.sublayerTransform = perspective;
    
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewTouch:)];
    [self.view addGestureRecognizer:pan];
    
}


- (void)viewTouch:(UIPanGestureRecognizer *)gesture{
    CGPoint point = [gesture translationInView:self.view];
    CGFloat angleX = self.diceAngle.x + (point.x / 30);
    CGFloat angleY = self.diceAngle.y - (point.y / 30);
    
    CATransform3D transform =CATransform3DIdentity;
    transform.m34 = -1 / 500;
    transform = CATransform3DRotate(transform, angleX, 0, 1, 0);
    transform = CATransform3DRotate(transform, angleY, 1, 0, 0);
    self.anchorView.layer.sublayerTransform = transform;
    
    if (gesture.state == UIGestureRecognizerStateEnded) {
        self.diceAngle = CGPointMake(angleX, angleY);
    }
}


- (UIView *)anchorView{
    if (!_anchorView) {
        UIView *anchorV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 300)];
        anchorV.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0);
        _anchorView = anchorV;
    }
    return _anchorView;
}

@end
