//
//  ViewController.m
//  AnimationAnimation
//
//  Created by shinyway on 2018/9/27.
//  Copyright © 2018年 shinyway. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    UIView *_redView;
    UIView *_blueView;
    UIView *_anchorView;
    CGPoint angle;
    UIImageView *_ballView;
    CGFloat ballsub;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    angle = CGPointMake(0, 0);
    
    UIGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewTransform:)];
    [self.view addGestureRecognizer:pan];
    
    
    //添加视图
    [self.view addSubview:self.anchorView];
    [self.anchorView addSubview:self.redView];
    [self.anchorView addSubview:self.blueView];
    
    
    //为redview添加transform
    CATransform3D transform  = CATransform3DIdentity;
    //添加透视
    transform.m34 = - 1.0 / 500.0;
    //添加旋转
    transform = CATransform3DRotate(transform, M_PI_4, 0, 1, 0);
    self.redView.layer.transform = transform;
    
    //为blueview添加transform
    CATransform3D transform2  = CATransform3DIdentity;
    //添加透视
    transform2.m34 = - 1.0 / 500.0;
    //添加旋转
    transform2 = CATransform3DRotate(transform2, M_PI_4, 0, -1, 0);
    self.blueView.layer.transform = transform2;
    
    
//    UIButton *btn = [UIButton buttonWithType:UIButtonTypeContactAdd];
//    btn.center = CGPointMake(100, 100);
//    [btn addTarget:self action:@selector(showAnimation) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:btn];
    
    
    
}

- (void)showAnimation{
    __weak __typeof(self)weakSelf = self;
    [UIView animateWithDuration:1.0f animations:^{
//        weakSelf.redView.transform = CGAffineTransformMake(2, 0, 0, 2, 100, 100);
        CATransform3D transform = CATransform3DIdentity;
        CGFloat angle = 45;
        weakSelf.redView.layer.transform = CATransform3DRotate(transform, angle, 1, 0, 1);
        weakSelf.blueView.layer.transform = CATransform3DRotate(transform, angle, 1, 0, 0);
    }];
}

- (UIView *)redView{
    if (!_redView) {
        UIView *redView = [[UIView alloc] initWithFrame:CGRectMake(50, 0, 100, 100)];
        redView.backgroundColor = [UIColor redColor];
        _redView = redView;
    }
    return _redView;
    
}

- (UIView *)blueView{
    if (!_blueView) {
        UIView *blueView = [[UIView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-100-50, 0, 100, 100)];
        blueView.backgroundColor = [UIColor blueColor];
        _blueView = blueView;
    }
    return _blueView;
}

- (UIView *)anchorView{
    if (!_anchorView) {
        UIView *anchorV = [[UIView alloc] initWithFrame:CGRectMake(0, 100, [UIScreen mainScreen].bounds.size.width, 300)];
        _anchorView = anchorV;
    }
    return _anchorView;
}


- (UIImageView *)ballView{
    if (!_ballView) {
        UIImageView *ballview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"4abae00be55f5223b3e4f19ca771dbb7"]];
        _ballView = ballview;
    }
    return _ballView;
}

- (void)viewTransform:(UIPanGestureRecognizer *)sender{
    __weak __typeof(self)weakSelf = self;
    CGPoint point = [sender translationInView:self.view];
    CGFloat anglex = angle.x + (point.x/30);
    CGFloat angley = angle.y - (point.y/30);
    NSLog(@"anglex%lf angley%lf",point.x, point.y);
    
    
    CATransform3D transform = CATransform3DIdentity;
//    transform.m34 = -1 / 500;
//    transform = CATransform3DRotate(transform, anglex, 0, 1, 0);
//    transform = CATransform3DRotate(transform, angley, 1, 0, 0);
//    weakSelf.redView.layer.transform = transform;
////    weakSelf.redView.layer.sublayerTransform = transform;
//
//    if (sender.state == UIGestureRecognizerStateEnded) {
//        angle = CGPointMake(anglex, angley);
//    }
    ballsub = 1-(point.x/[UIScreen mainScreen].bounds.size.width);
    NSLog(@"ballsub%lf",ballsub);
    transform = CATransform3DScale(transform, ballsub, ballsub, 1);
    transform = CATransform3DTranslate(transform, point.x, 0, 0);
    
    weakSelf.ballView.layer.transform = transform;
    if (sender.state == UIGestureRecognizerStateEnded) {
        angle = CGPointMake(anglex, angley);
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
