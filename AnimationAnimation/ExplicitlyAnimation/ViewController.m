//
//  ViewController.m
//  ExplicitlyAnimation
//
//  Created by shinyway on 2018/11/7.
//  Copyright © 2018年 shinyway. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
/** colorLayer */
@property(nonatomic, strong)CALayer *colorLayer;
/** CALayer *arrow */
@property(nonatomic, strong)CALayer *arrow;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.colorLayer = [CALayer layer];
    self.colorLayer.frame = CGRectMake(0, 0, 100, 100);
    self.colorLayer.position = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
    [self.view.layer addSublayer:self.colorLayer];
    
    
    //keyframeAnimation
    //draw the path using a CAShapeLayer
    /*CAShapeLayer *pathLayer = [CAShapeLayer layer];
    pathLayer.path = flyWayPath.CGPath;
    pathLayer.fillColor = [UIColor clearColor].CGColor;
    pathLayer.strokeColor = [UIColor redColor].CGColor;
    pathLayer.lineWidth = 3.0f;
    [self.view.layer addSublayer:pathLayer];*/
    
    //add arrow
    self.arrow = [CALayer layer];
    self.arrow.contents = (__bridge id)[UIImage imageNamed:@"icon_next_blue@3x"].CGImage;
    self.arrow.frame = CGRectMake(0, 0, 20, 20);
    self.arrow.position = CGPointMake(50, 200);
    [self.view.layer addSublayer:self.arrow];
    
    
    
}


- (IBAction)changeColor:(id)sender {
    CABasicAnimation *colorAnima = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
    CGFloat red = arc4random() / (CGFloat)INT_MAX;
    CGFloat green = arc4random() / (CGFloat)INT_MAX;
    CGFloat blue = arc4random() / (CGFloat)INT_MAX;
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1];
    colorAnima.toValue = (__bridge id)color.CGColor;
    [self.colorLayer addAnimation:colorAnima forKey:nil];

    //将实际数据改为终值
    self.colorLayer.backgroundColor = color.CGColor;
    
    
    //add animation
    UIBezierPath *flyWayPath = [[UIBezierPath alloc] init];
    [flyWayPath moveToPoint:CGPointMake(50, 200)];
    [flyWayPath addCurveToPoint:CGPointMake(400, 200) controlPoint1:CGPointMake(100, 200) controlPoint2:CGPointMake(150, 100)];
    
    CAKeyframeAnimation *moveAnima = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    moveAnima.path = flyWayPath.CGPath;
    moveAnima.duration = 2.0;
    moveAnima.rotationMode = kCAAnimationRotateAuto;
    [self.arrow addAnimation:moveAnima forKey:nil];
    
    self.arrow.position = CGPointMake(400, 200);
}




@end
